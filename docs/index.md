# Expérience avec MkDocs et Pyodide

Ce dépôt est à cloner et adapter à votre besoin.


## Prérequis

Avec le fichier `requirements.txt` suivant

```txt title="requirements.txt"
mkdocs-material
mkdocs-macros-plugin
mkdocs-awesome-pages-plugin
selenium
mkdocs-exclude-search
```

Vérifier l'installation des dépendances avec

```console
$ pip install -r requirements.txt
```

